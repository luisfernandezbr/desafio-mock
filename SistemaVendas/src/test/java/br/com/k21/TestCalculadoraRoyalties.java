package br.com.k21;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.k21.dao.VendaRepository;
import br.com.k21.modelo.Venda;

// Lula
// Cuba
// Alberto Marx


public class TestCalculadoraRoyalties {

	@Test
	public void teste_mes_sem_vendas_retorna_0() {

		int mes = 7;
		int ano = 2019;
		int result = 0;

		double royaltiesCalculado = new CalculadoraRoyalties().calcularRoyalties(mes, ano);

		assertEquals(result, royaltiesCalculado,0);
	}
	
	
	@Test
	public void teste_mes_03_de_2019_com_uma_venda_de_2045_retorna_388_55() {

		int mes = 3;
		int ano = 2019;
		double result = 388.55;

		VendaRepository marionete = Mockito.mock(VendaRepository.class);
		
		List<Venda> listaFalsa = new ArrayList<Venda>();
		
		listaFalsa.add(new Venda(3, 1, mes, ano, 2045));
		
		Mockito.when(marionete.obterVendasPorMesEAno(ano, mes)).thenReturn(listaFalsa);
		
		double royaltiesCalculado = new CalculadoraRoyalties(marionete).calcularRoyalties(mes, ano);

		assertEquals(result, royaltiesCalculado,0);
	}
	
	@Test
	public void teste_mes_03_de_2021_com_total_de_vendas_de_402480_34_retorna_75670_62() {

		int mes = 3;
		int ano = 2021;
		double result = 75670.62;

		double royaltiesCalculado = new CalculadoraRoyalties().calcularRoyalties(mes, ano);

		assertEquals(result, royaltiesCalculado,0);
	}
}
